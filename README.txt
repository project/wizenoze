Wizenoze
---------------------------------


This module allows you to  
a. Configure Wizenoze API, collection and custom search engine 
b. Create simple search pages and search blocks for searching in your 
Wizenoze API indexes, similar to the core Search functionality.

Installation
------------

As the only pre-requisite you will need the Wizenoze API key to enable and
correctly set up. Create a collection for content ingestion and custom 
search engine according to your needs.


Configuration
-------------

To add and configure a Wizenoze, go to /admin/config/wizenoze/settings
and add a new page.

[1] https://drupal.org/project/wizenoze

API Document
--------------
http://docs.wizenozecontentcollection.apiary.io/
https://market.mashape.com/wizenoze
